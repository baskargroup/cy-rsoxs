CyRSoXS: GPU enabled RSoXS simulation
====================================

![](docs/schematic.png){height=0 width=0} 

* This code solves the X-ray scattering on Graphical processing units (GPU).
Currently it supports the operation with 32 bit / 64 bit float datatypes.

* The code can be executed through the Python interface (enabled through [Pybind11](https://github.com/pybind/pybind11)) or directly through the executable.



Version  : 1.1.4.0 - Beta
================
* Note:  This is the development version of CyRSoXS. For the latest stable version, please visit [https://github.com/usnistgov/cyrsoxs](https://github.com/usnistgov/cyrsoxs).

Dependencies
=============
#### Required Dependencies
* A C++ compiler with C++14 support is required.
* gcc >= 7 (CUDA specific versions might have GCC requirements )
* Cuda Toolkit (>=9)
* HDF5
* OpenMP

#### Additional dependencies for building with Pybind
* Python >= 3.6

#### Additional dependencies for building without Pybind
* libconfig

#### Optional Dependencies
* Doxygen
* Docker

Running CyRSoXS with docker
============================

The docker for CyRSoXS is available at [Docker Hub](https://hub.docker.com/repository/docker/maksbh/cy-rsoxs/general).
This comes installed with nvidia toolkit and other dependencies.
See [docs/DOCKER.md](docs/DOCKER.md) for detailed instructions on how to build CyRSoXS with the docker image.


Installation Instruction without Docker
======================================
See [docs/INSTALL.md](docs/INSTALL.md) for detailed instructions on how to build CyRSoXS and its dependencies.

Running CyRSoXS
================
See [docs/RUN.md](docs/RUN.md) for detailed instructions on how to run CyRSoXS.


Contributors
============
* Kumar Saurabh
* Adarsh Krishnamurthy
* Baskar Ganapathysubramanian
* Eliot Gann
* Dean Delongchamp
* Michael Chabinyc

Acknowledgement
===============
We thank ONR MURI Center for Self-Assembled Organic Electronics for providing the support for this work.

Frequently Asked Question
============================
See [docs/FAQ.md](docs/FAQ.md) for some of the known issues or Frequently asked questions.

License
====================
The code is released in accordance with NIST License. Please read the terms and condition [here](docs/LICENSE.md). 

Contact
=======
Questions and comments should be sent to Dr. Baskar Ganapathysubramanian at [baskarg@iastate.edu](mailto:baskarg@iastate.edu) or Dr.  Adarsh Krishnamurthy [adarsh@iastate.edu](mailto:adarsh@iastate.edu).
